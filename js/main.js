const googleScriptLink = "https://script.google.com/macros/s/AKfycbxJkiEvXx1WwG6kLl2agTCap5gppFFqD6hjFwTKX6Wz5wNCUSrpEtp9ItxuzOTWHryfBA/exec";
const googleTranslateScript = 'https://script.google.com/macros/s/AKfycbzGH-KOkKq0yq3neti4GQpWERAkO8VMlfSTYB_Er_qWlTpsEKhw/exec';
const sheetUrl = "https://spreadsheets.google.com/feeds/list/";
const sheetParam = "/public/values?alt=json&sq=";
const spreadsheetID = "1kwkjz-iPvmK2Sm445ogE4yUAuwZR8E3QONCLpxQRCWA";
const googleTranslate = 'https://translate.google.com/translate?sl=auto&tl=en&u=';

// Ctrl + K + 0

async function test() {
    const countDown = $('#countdown_timer')[0];

    var observer = new MutationObserver(() => {
        var time = $('.countdown_amount').text();
        console.log(time);
    });

    if (countDown) {
        observer.observe(countDown, {
            childList: true,
            subtree: true
        });
    } else {
    }
}

function callAjax(index) {
    const url = chrome.runtime.getURL('data/data.json');
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        selectChangeDOM(index, data);
    }).fail(function (e) {
        updateCount('error');
    });
    scrollToEnd();
}

function selectChangeDOM(index, data) {
    switch (index) {
        case 0:
            changeDOMInWhatIndustryThisCompanyWork(data[0].InWhatIndustryThisCompanyWork);
            break;
        case 1:
            changeDOMWhereIsThisPublisherFrom(data[1].WhereIsThisPublisherFrom);
            break;
        default:
            break;
    }
}

function changeDOMInWhatIndustryThisCompanyWork(data) {
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var url = $(item).find('.accounts-table').find('a').attr('href');
        var value = getValueInWhatIndustryThisCompanyWork(data, url);
        $(item).find('.industry').val(value);
    });
}

function changeDOMWhereIsThisPublisherFrom(data) {
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var url = $(item).find('.accounts-table').find('a').attr('href');
        var value = getValueWhereIsThisPublisherFrom(data, url);
        $(item).find('.country').val(value);
    });
}

function getValueInWhatIndustryThisCompanyWork(data, url) {
    var value = findRecord(data, url);
    if (!value) {
        var i = randomNumber();
        if (i < 3) {
            value = '3';
        } else if (i < 6) {
            value = '14';
        } else {
            value = '8';
        }
    }
    return value;
}

function getValueWhereIsThisPublisherFrom(data, url) {
    var value = findRecord(data, url);
    if (!value) {
        var i = randomNumber();
        if (i < 3) {
            value = 'CA';
        } else if (i < 5) {
            value = 'GB';
        } else {
            value = 'US';
        }
    }
    return value;
}

function findRecord(data, url) {
    for (const i in data) {
        if (data[i][url] != null) {
            return data[i][url];
        }
    }
    return null;
}

function randomNumber() {
    return Math.floor((Math.random() * 10) + 1);
}

// ---------------------------------------------------AMAZON------------------------------------------------------------

async function completeTaskAmazon() {
    var sheetId = 'o7lxt36';
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('a').removeClass("validates-clicked");
        const searchUrl = $(item).find('a').first().attr('href');
        const searchQuery = 'url==' + encodeURIComponent('\"' + searchUrl + '\"');
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;

        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                updateCount('loop');
                var type1 = true;
                var type2 = true;
                if (entry[0].gsx$type1.$t === 'false') {
                    type1 = false;
                }
                if (entry[0].gsx$type2.$t === 'false') {
                    type2 = false;
                }
                changeDOMAmazon(entry[0].gsx$isbn1.$t, entry[0].gsx$rank1.$t, type1, 0, index);
                changeDOMAmazon(entry[0].gsx$isbn2.$t, entry[0].gsx$rank2.$t, type2, 1, index);
            } else {
                setDefaultDataAmazon(index);
                getGooglePage(searchUrl, index);
            }
        });
    });
}

async function setDefaultDataAmazon(rootIndex) {
    changeDOMAmazon(null, null, false, 0, rootIndex);
    changeDOMAmazon(null, null, false, 1, rootIndex);
}

async function getGooglePage(url, rootIndex) {
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        var url = $(data).find(".g").first().find('a').attr('href');
        getAmazonPage(url, rootIndex);
    }).fail(function (e) {
        updateCount('error');
    });
}

async function getAmazonPage(rootUrl, rootIndex) {
    $.ajax({
        type: "GET",
        url: rootUrl
    }).done(function (data) {
        let url;
        updateCount('loop');
        let hasData = false;
        $(data).find('.swatchElement').each(function (index, item) {
            let url;
            const text = $(item).find('a').first().find('span').first().text().replace(/ /g, '');
            if (text === 'Hardcover') {
                hasData = true;
                url = $(item).find('a').first().attr('href');
                if (url === 'javascript:void(0)') {
                    getDataAmazon(rootUrl, 'Hardcover', rootIndex);
                } else {
                    getDataAmazon('https://www.amazon.com' + url, 'Hardcover', rootIndex);
                }
            }
            if (text === 'Paperback') {
                hasData = true;
                url = $(item).find('a').first().attr('href');
                if (url === 'javascript:void(0)') {
                    getDataAmazon(rootUrl, 'Paperback', rootIndex);
                } else {
                    getDataAmazon('https://www.amazon.com' + url, 'Paperback', rootIndex);
                }
            }
        })
        if (!hasData) {
            const listChild = $(data).find('#mediaTabs_tabSet').children();
            for (const item of listChild) {
                var text = $(item).find('.mediaTab_title').text();
                if (text === 'Hardcover') {
                    url = $(item).find('a').first().attr('href');
                    getDataAmazon('https://www.amazon.com' + url, 'Hardcover', rootIndex);
                }
                if (text === 'Paperback') {
                    url = $(item).find('a').first().attr('href');
                    getDataAmazon('https://www.amazon.com' + url, 'Paperback', rootIndex);
                }
            }
        }
    }).fail(function (e) {
        updateCount('error');
    });
}

async function getDataAmazon(url, type, rootIndex) {
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        let ISBN = null;
        let rank = null;
        let ISBNElement = null;
        let rankElement = null;
        if (type === 'Hardcover' || type === 'Paperback') {
            ISBNElement = $(data).find('li:contains("ISBN-10")').find('div').last().find('span');
            ISBN = $(ISBNElement).text().replace(':', '').replace('ISBN-10', '').trim();
            rankElement = $(data).find('span:contains("in Books (")');
        } else {
            ISBNElement = $(data).find('li:contains("ISBN-13")');
            ISBN = $(ISBNElement).text().replace(':', '').replace('ISBN-13', '').trim();
            rankElement = $(data).find('span:contains("en Libros (")');
        }
        if (!ISBN) {
            ISBNElement = $(data).find('li:contains("ASIN")');
            ISBN = $(ISBNElement).text().replace(':', '').replace('ASIN', '').trim();
        }
        if (!rank) {
            let text = $(rankElement).text().replace(/.*:/gi, '').trim().replace(/(\r\n|\n|\r)/gm, "");
            console.log(text.indexOf(' in Books ('));
            text = text.substr(0, text.indexOf(' ')).replace(/[^0-9]/gi, '');
            rank = text;
        }
        if (type === 'Hardcover' || type === 'Pastadura') {
            changeDOMAmazon(ISBN, rank, true, 0, rootIndex);
        } else if (type === "Paperback" || type === "Pastablanda") {
            changeDOMAmazon(ISBN, rank, true, 1, rootIndex);
        }
    }).fail(function (e) {
        updateCount('error');
    });
}

async function changeDOMAmazon(ISBN, rank, haveBook, type, rootIndex) {
    // console.log(ISBN + '-' + rank + '-' + haveBook + '-' + type + '-' + rootIndex);
    $('div[class="cml jsawesome"]').each(function (index, item) {
        if (index === rootIndex) {
            if (!haveBook) {
                if (type === 0) {
                    $(item).find('.hardcover_found_yn').last().click();
                } else {
                    $(item).find('.paperback_found_yn').last().click();
                }
            } else {
                if (type === 0) {
                    $(item).find('.hardcover_found_yn').first().click();
                    if (rank) {
                        $(item).find('.hardcover_rank_yn').first().click();
                        $(item).find('.hardcover_rank').val(rank);
                    } else {
                        $(item).find('.hardcover_rank_yn').last().click();
                    }
                    if (ISBN) {
                        $(item).find('.hardcover_isbn_yn').first().click();
                        $(item).find('.hardcover_isbn10').val(ISBN);
                    } else {
                        $(item).find('.hardcover_isbn_yn').last().click();
                    }
                } else if (type === 1) {
                    $(item).find('.paperback_found_yn').first().click();
                    if (rank) {
                        $(item).find('.paperback_rank_yn').first().click();
                        $(item).find('.paperback_rank').val(rank);
                    } else {
                        $(item).find('.paperback_rank_yn').last().click();
                    }
                    if (ISBN) {
                        $(item).find('.paperback_isbn_yn').first().click();
                        $(item).find('.paperback_isbn10').val(ISBN);
                    } else {
                        $(item).find('.paperback_isbn_yn').last().click();
                    }
                }
            }
        }
    });
}

async function getAnswerAmazon() {
    const object = {};
    object.sheetName = "Amazon";
    object.size = 7;
    object.encode = true;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchUrl = $(item).find('a').first().attr('href');

        var key = [];
        key.push(encodeURIComponent(searchUrl));

        var arr = [];
        if ($(item).find('.hardcover_found_yn:checked').val() === 'no') {
            arr.push('false');
            arr.push(null);
            arr.push(null);
        } else {
            arr.push('true');
            if ($(item).find('.hardcover_isbn_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.hardcover_isbn10').val());
            }
            if ($(item).find('.hardcover_rank_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.hardcover_rank').val());
            }
        }
        if ($(item).find('.paperback_found_yn:checked').val() === 'no') {
            arr.push('false');
            arr.push(null);
            arr.push(null);
        } else {
            arr.push('true');
            if ($(item).find('.paperback_isbn_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.paperback_isbn10').val());
            }
            if ($(item).find('.paperback_rank_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.paperback_rank').val());
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });

    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------------------------MEXICO------------------------------------------------------------

async function completeTaskAmazonMexico() {
    var sheetId = 'o7lxt36';
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('a').removeClass("validates-clicked");
        var searchUrl = $(item).find('a').first().attr('href');
        var searchQuery = 'url==' + encodeURIComponent('\"' + searchUrl + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;


        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                updateCount('loop');
                var type1 = true;
                var type2 = true;
                if (entry[0].gsx$type1.$t === 'false') {
                    type1 = false;
                }
                if (entry[0].gsx$type2.$t === 'false') {
                    type2 = false;
                }
                changeDOMAmazon(entry[0].gsx$isbn1.$t, entry[0].gsx$rank1.$t, type1, 0, index);
                changeDOMAmazon(entry[0].gsx$isbn2.$t, entry[0].gsx$rank2.$t, type2, 1, index);
            } else {
                setDefaultDataAmazon(index);
                getGooglePageMexico(searchUrl, index);
            }
        });
    });
}

async function getGooglePageMexico(rootURL, rootIndex) {
    $.ajax({
        type: "GET",
        url: rootURL
    }).done(function (data) {
        var item;
        var index = 0;
        var urlValue;
        while (true) {
            item = $(data).find(".s-desktop-width-max").last().find('div[data-index =' + index + ']');
            var dataAsin = $(item).attr('data-asin');
            if (dataAsin && dataAsin != '') {
                urlValue = $(item).find('a').first().attr('href');
                break;
            } else if (index < 10) {
                index++;
            } else {
                break;
            }
        }
        if (!urlValue) {
            index = 0;
            var urlValue;
            while (!urlValue) {
                item = $(data).find(".s-desktop-width-max.s-opposite-dir").find("div[data-index ='" + index + "']");
                urlValue = $(item).find('a').first().attr('href');
                index++;
                if (index == 10) {
                    break;
                }
            }
        }
        if (urlValue) {
            getAmazonMexicoPage('https://www.amazon.com.mx' + urlValue, rootIndex);
        } else {
            console.log("fail");
            getGooglePageMexico(rootURL, rootIndex);
        }
    }).fail(function (e) {
        updateCount('error');
    });
}

async function getAmazonMexicoPage(rootUrl, rootIndex) {
    $.ajax({
        type: "GET",
        url: rootUrl
    }).done(function (data) {
        updateCount('loop');
        $(data).find('.swatchElement').each(function (index, item) {
            var text = $(item).find('a').first().find('span').first().text().replace(/ /g, '');
            if (text == 'Pastadura') {
                var url = $(item).find('a').first().attr('href');
                if (url == 'javascript:void(0)') {
                    getDataAmazon(rootUrl, 'Pastadura', rootIndex);
                } else {
                    getDataAmazon('https://www.amazon.com.mx' + url, 'Pastadura', rootIndex);
                }
            }
            if (text == 'Pastablanda') {
                var url = $(item).find('a').first().attr('href');
                if (url == 'javascript:void(0)') {
                    getDataAmazon(rootUrl, 'Pastablanda', rootIndex);
                } else {
                    getDataAmazon('https://www.amazon.com.mx' + url, 'Pastablanda', rootIndex);
                }
            }
        })
    }).fail(function (e) {
        updateCount('error');
    });
}

async function updateCount(type) {
    switch (type) {
        case 'loop':
            var loop = Number($('#loop').text()) + 1;
            $('#loop').html(loop);
            break;
        case 'error':
            var error = Number($('#error').text()) + 1;
            $('#error').html(error);
            break;
        default:
            break;
    }
}

async function getAnswerMexico() {
    var object = new Object();
    object.sheetName = "Amazon";
    object.size = 7;
    object.encode = true;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchUrl = $(item).find('a').first().attr('href');

        var key = [];
        key.push(encodeURIComponent(searchUrl));

        var arr = [];
        if ($(item).find('.hardcover_found_yn:checked').val() === 'no') {
            arr.push('false');
            arr.push(null);
            arr.push(null);
        } else {
            arr.push('true');
            if ($(item).find('.hardcover_isbn_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.hardcover_isbn10').val());
            }
            if ($(item).find('.hardcover_rank_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.hardcover_rank').val());
            }
        }
        if ($(item).find('.paperback_found_yn:checked').val() === 'no') {
            arr.push('false');
            arr.push(null);
            arr.push(null);
        } else {
            arr.push('true');
            if ($(item).find('.paperback_isbn_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.paperback_isbn10').val());
            }
            if ($(item).find('.paperback_rank_yn:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.paperback_rank').val());
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });

    var dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ----------------------------------------------------LOOK-------------------------------------------------------------

async function completeTaskLook() {
    changeDomLook(null, null);
    const key = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let searchUrl = $(item).find('a').first().attr('href');
        searchUrl = searchUrl.replace(/.*\//, '');
        const itemKey = [];
        itemKey.push(searchUrl);
        key.push(itemKey);
    });
    const promise = getAnswer("Look", key);
    promise.done(function (data) {
        $('div[class="cml jsawesome"]').each(function (index, item) {
            let searchUrl = $(item).find('a').first().attr('href');
            searchUrl = searchUrl.replace(/.*\//, '');
            $.each(data, function (i, val) {
                if (val[0] === searchUrl) {
                    $(item).css('background-color', 'yellow');
                    changeDomLook(index, val[1]);
                }
            })
        });
    })
}

async function changeDomLook(index, value) {
    if (index == null) {
        $('div[class="cml jsawesome"]').each(function (index, item) {
            $(item).find('.advertiser_ok[value="yes"]').click();
            $(item).find('.store[value="yes"]').click();
            $(item).find('.department[value="one"]').click();
            const noAdvertiser = $(item).find('.no_advertiser').first();
            if (!$(noAdvertiser).parents('div.radios').hasClass('_cf_hidden')) {
                hideOptionLook(item, '.no_advertiser');
                hideOptionLook(item, '.brand');
                hideOptionLook(item, '.product');
                hideOptionLook(item, '.num_products');
                hideOptionLook(item, '.product_dom');
                hideOptionLook(item, '.website');
                hideOptionLook(item, '.event');
                hideOptionLook(item, '.sponsor');
                hideOptionLook(item, '.num_sponsors');
                hideOptionLook(item, '.sponsor_dom');
                hideOptionLook(item, '.brand_product_store');
            }
            $(item).find('.group.logic-only-if.well').each(function (id, it) {
                $(it).addClass("_cf_hidden");
            })
        });
    } else {
        const item = $('div[class="cml jsawesome"]')[index];
        if (value === 'No') {
            const temp = $(item).find('.advertiser_ok')[1].click();
            $(item).find('.no_advertiser[value="no_logo"]').click();
        } else if (value === 'null') {

        } else {
            $('<span style="font-size: 30px; color: red;" class="answer">' + value + '</span><br/>').insertBefore($(item).find('.adfor'));
        }
    }
}

async function hideOptionLook(item, className) {
    const element = $(item).find(className).first();
    $(element).parents('div.radios').addClass('_cf_hidden');
    $(element).click();
}

async function setSecondInputLook(target) {
    const item = $(target).parents('div.jsawesome');
    let secondInput = $(item).find('.notonlist_input').first();
    $(secondInput).val($(target).text());
    $(secondInput).addClass('validation-passed');
    $(secondInput).removeClass('has_default');
}

async function getAnswerLook(error) {
    $(error).find('.missed').each(function (index, item) {
        const answerElement = $(item).find('.unit_message').first();
        const text = $(answerElement).text().trim();
        let value;
        if (/For the question titled "Write the primary Advertiser, Brand or Product\?" you answered:/.test(text)
            || /For the question titled "Qual é o principal anunciante, marca ou produto\?" you answered/.test(text)) {
            value = $(answerElement).find('span').last().text().replace(/"/g, '');
            console.log('yes');
        }
        if (/For the question titled "Were you able to find any advertiser info in this ad/.test(text)
            || /For the question titled "O anúncio possui informações que te permitem identificar qual é o anunciante\?/.test(text)) {
            console.log('no');
            value = 'No';
        }
        const url = $(item).find('a').first().attr('href').replace(/.*\//, '');
        updateAnswerLook(url, value);
    });
}

async function updateAnswerLook(url, value) {
    const object = {};
    object.sheetName = "Look";
    object.size = 2;
    object.data = [];
    object.key = [];

    object.key.push([url]);
    object.data.push([value]);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: JSON.stringify(object),
        async: true,
        dataType: 'json'
    });
}

// ----------------------------------------------------PROD-------------------------------------------------------------

async function callAjaxProd(index) {
    const url = chrome.runtime.getURL('data/prod.json');
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        completeTaskProd(index, data);
    }).fail(function (e) {
        console.log(e);
    });
}

async function completeTaskProd(rootIndex, data) {
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('.linkedin_found_yn').first().click();
        var title = $(item).find('strong').first().text();
        var url = $(item).find('a').first().attr('href');
        $(item).find('a').each(function (id, i) {
            $(i).removeClass('validates-clicked')
        });
        if (rootIndex == 6) {
            getDataProdPerson(item, title, url, data[0].Person);
        } else {
            getDataProdAccount(item, title, data[1].Account);
        }
    });
}

async function getDataProdPerson(item, title, rootUrl, data) {
    var value = findRecord(data, title);
    if (value) {
        changeDomProdPerson(item, value.url, value.work);
    } else {
        $.ajax({
            type: "GET",
            url: rootUrl
        }).done(function (data) {
            var url = $(data).find(".g").first().find('.rc').find('a').attr('href');
            changeDomProdPerson(item, url, true);
        }).fail(function (e) {
            console.log('error google prod');
        });
    }
}

async function getDataProdAccount(item, title, data) {
    var value = findRecord(data, title);
    if (value) {
        changeDomProdAccount(item, value.industry, value.website, value.select, value.size, value.subSize);
    } else {
        changeDomProdAccount(item, true, true, null, true, true);
    }
}

async function changeDomProdPerson(item, url, work) {
    if (!url) {
        $(item).find('.linkedin_found_yn').last().click();
    } else {
        $(item).find('.linkedin_url').val(url);
        if (work) {
            $(item).find('.still_working_at_company').first().click();
            $(item).find('.job_title_unchanged').first().click();
        } else {
            $(item).find('.still_working_at_company').last().click();
        }
    }
}

async function changeDomProdAccount(item, industry, website, select, size, subSize) {
    $(item).find('.industry_match').first().click();
    $(item).find('.company_size_employee_range_match').first().click();
    $(item).find('.google_address_match').first().click();
    if (!industry) {
        $(item).find('.industry_match').last().click();
        $(item).find('.website_industry_match').first().click();
        if (!website) {
            $(item).find('.website_industry_match').last().click();
            $(item).find('.linkedin_company_industry').val(select);
        }
    }
    if (!size) {
        $(item).find('.company_size_employee_range_match').last().click();
        $(item).find('.employee_num_match').first().click();
        if (subSize != null) {
            $(item).find('.employee_num_match').last().click();
            $(item).find('.num_employees ').val(subSize);
        }
    }
}

// --------------------------------------------------DOCUMENT-----------------------------------------------------------

async function completeTaskDocument() {
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('.yes_bol').first().click();
        $(item).find('.bol_number').val('042850101');
        $(item).find('.delivery_sticker_present').last().click();
    });
}

// --------------------------------------------------IDENTIFY-----------------------------------------------------------

async function completeTaskIdentifyImage() {
    var sheetId = 'onsamy6';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchId = $(item).attr('id');
        var url = "https://spreadsheets.google.com/feeds/list/" + spreadsheetID + "/" + sheetId +
            "/public/values?alt=json&sq=id==" + searchId;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.please_select_the_tops_pattern_which_is_visible_in_the_image').val(entry[0].gsx$value.$t);
            }
        });
    });
}

// ---------------------------------Help Us Find Official/Directory Websites--------------------------------------------

async function completeTaskHelpUsFindOfficial() {
    const sheetId = 'o1xinkl';
    await changeDomLook(null, null);
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('.html.cml_field').find('a').first().removeClass('validates-clicked');
        $(item).find('.html.cml_field').find('a').last().removeClass('validates-clicked');
        $(item).find('.official_yn').first().click();
        $(item).find('.directory_yn').first().click();
        let name = $(item).find('span').first().text();
        name = encodeURIComponent('\"' + name + '\"');
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "name==" + name;
        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$official.$t == 'No' || entry[0].gsx$official.$t == 'no') {
                    $(item).find('.official_yn').last().click();
                } else {
                    $(item).find('.official_url').val(entry[0].gsx$official.$t);
                }
                if (entry[0].gsx$directory.$t == 'No' || entry[0].gsx$directory.$t == 'no') {
                    $(item).find('.directory_yn').last().click();
                } else {
                    $(item).find('.directory_url').val(entry[0].gsx$directory.$t);
                }
            }
        });
    });
}

// ------------------------------------------------Trading Cart---------------------------------------------------------

async function completeTradingCard() {
    showSubmit();
    var sheetId = 'o8es1mi';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchId = $(item).attr('id');
        showId(item, searchId);
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "id==" + searchId;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.quality_issues[value="' + entry[0].gsx$one.$t + '"]').click();
                $(item).find('.one_card[value="' + entry[0].gsx$two.$t + '"]').click();
                $(item).find('.only_art[value="' + entry[0].gsx$three.$t + '"]').click();
                $(item).find('.first_match[value="' + entry[0].gsx$four.$t + '"]').click();
                $(item).find('.second_match[value="' + entry[0].gsx$five.$t + '"]').click();
                $(item).find('.third_match[value="' + entry[0].gsx$six.$t + '"]').click();
            } else {
                $(item).find('.quality_issues').last().click();
                $(item).find('.one_card').first().click();
                $(item).find('.only_art').first().click();
                $(item).find('.first_match').first().click();
                $(item).find('.second_match').first().click();
                $(item).find('.third_match').first().click();
            }
        });
    });
}

async function getDataTradingCard() {
    var object = new Object();
    object.sheetName = "Trading Cards";
    object.size = 7;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var id = $(item).attr('id');

        var key = [];
        key.push(id);

        var arr = [];
        arr.push($(item).find('.quality_issues:checked').val());
        arr.push($(item).find('.one_card:checked').val());
        arr.push($(item).find('.only_art:checked').val());
        arr.push($(item).find('.first_match:checked').val());
        arr.push($(item).find('.second_match:checked').val());
        arr.push($(item).find('.third_match:checked').val());

        object.key.push(key);
        object.data.push(arr);
    });

    var dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------------------Product Substitution----------------------------------------------------

async function completeProductSubstitution() {
    var sheetId = 'ogb9hpz';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('.based_on_the_above_information_do_both_items_represent_substitute_products')[1].click();
        var id = $(item).attr('id');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "id==" + id;
        $('<span style="font-size: 16px; color: red;" class="answer">' + id + '</span><br/>').insertBefore($(item).find('.legend'));
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.based_on_the_above_information_do_both_items_represent_substitute_products[value="' + entry[0].gsx$value.$t + '"]').click();
            }
        });
    });
}

// ------------------------------------------------------Dup------------------------------------------------------------

async function completeTaskDup() {
    showSubmit();
    var sheetId = 'ovyft54';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var urlImg = $(item).find('.span2.item.well.well-sm').find('img').attr('src');
        urlImg = encodeURIComponent('\"' + urlImg + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "url==" + urlImg;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$answer1.$t == 'none') {
                    $(item).find('img').last().click();
                } else {
                    if (entry[0].gsx$answer1.$t) {
                        $(item).find('img[src="' + entry[0].gsx$answer1.$t + '"]').click();
                    }
                    if (entry[0].gsx$answer2.$t) {
                        $(item).find('img[src="' + entry[0].gsx$answer2.$t + '"]').click();
                    }
                    if (entry[0].gsx$answer3.$t) {
                        $(item).find('img[src="' + entry[0].gsx$answer3.$t + '"]').click();
                    }
                    if (entry[0].gsx$answer4.$t) {
                        $(item).find('img[src="' + entry[0].gsx$answer4.$t + '"]').click();
                    }
                    if (entry[0].gsx$answer5.$t) {
                        $(item).find('img[src="' + entry[0].gsx$answer5.$t + '"]').click();
                    }
                    if (entry[0].gsx$answer6.$t) {
                        $(item).find('img[src="' + entry[0].gsx$answer6.$t + '"]').click();
                    }
                }
            }
        });
    });
}

async function getAnswerDup() {
    var object = new Object();
    object.sheetName = "Dup";
    object.size = 7;
    object.data = [];
    object.key = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var taskUrl = $(item).find('.image_popover_bottom').attr('src');

        var key = [];
        key.push(taskUrl);

        var arr = [];
        var items = $(item).find('.cml_row');

        for (let index = 0; index < items.length; index++) {
            const element = items[index];
            var input = $(element).find('input').first().prop('checked');
            if (input) {
                var url = $(element).find('img').first().attr('src');
                if (url.startsWith('data:image')) {
                    arr.push('none');
                    break;
                } else {
                    arr.push(url);
                }
            }
        }
        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: JSON.stringify(object),
        async: false,
        dataType: 'json'
    });
}

// ----------------------------------------------------Address----------------------------------------------------------

async function completeTaskAddress() {
    showSubmit();
    const sheetId = 'or6qgas';
    const urlTranslate = [
        'start.cortera.com',
        'yellowpages.com',
        'owler.com',
        'superpages.com',
        'glassdoor.com'
    ];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('a').first().removeClass('validates-clicked');
        $(item).find('.street_missing').last().click();
        $(item).find('.city_missing').last().click();
        $(item).find('.state_missing').last().click();
        $(item).find('.postal_code_missing').last().click();
        let pageUrl = $(item).find('a').first().attr('href');
        pageUrl = encodeURIComponent('\"' + pageUrl + '\"');
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "website==" + pageUrl;
        $.getJSON(url, function (data) {
            let value;
            const entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$name.$t) {
                    value = (entry[0].gsx$name.$t).toLowerCase();
                    if (value !== 'yes') {
                        $(item).find('.business_name').val(entry[0].gsx$name.$t);
                    }
                } else {
                    $(item).find('.broken_link').click();
                }
                if (entry[0].gsx$address.$t) {
                    $(item).find('.street_missing').first().click();
                    value = (entry[0].gsx$address.$t).toLowerCase();
                    if (value !== 'yes') {
                        $(item).find('.street').val(entry[0].gsx$address.$t);
                    }
                }
                if (entry[0].gsx$city.$t) {
                    $(item).find('.city_missing').first().click();
                    value = (entry[0].gsx$city.$t).toLowerCase();
                    if (value !== 'yes') {
                        $(item).find('.city').val(entry[0].gsx$city.$t);
                    }
                }
                if (entry[0].gsx$state.$t) {
                    $(item).find('.state_missing').first().click();
                    value = (entry[0].gsx$state.$t).toLowerCase();
                    if (value !== 'yes') {
                        $(item).find('.state').val(entry[0].gsx$state.$t);
                    }
                }
                if (entry[0].gsx$code.$t) {
                    $(item).find('.postal_code_missing').first().click();
                    value = (entry[0].gsx$code.$t).toLowerCase();
                    if (value !== 'yes') {
                        $(item).find('.postal_code').val(entry[0].gsx$code.$t);
                    }
                }
            } else {
                const ulrTrans = $(item).find('a').first().attr('href');
                const domain = ulrTrans.replace('http://', '').replace('https://', '').split(/[/?#]/)[0];
                switch (domain) {
                    case 'dnb.com':
                        getDataDNB(ulrTrans, item);
                        break;
                    case 'mapquest.com':
                        getDataMapQuest(ulrTrans, item);
                        break;
                    case 'yelp.com':
                        getDataYelp(ulrTrans, item, 0);
                        break;
                    default: {
                        urlTranslate.forEach(function (it, i) {
                            if (it === domain) {
                                newUrl = googleTranslate + ulrTrans;
                                $(item).find('a').first().attr('href', newUrl);
                            }
                        });
                        $(item).find('.street_missing').first().click();
                        $(item).find('.city_missing').first().click();
                        $(item).find('.state_missing').first().click();
                        $(item).find('.postal_code_missing').first().click();
                    }
                }
            }
        });
    });
}

async function getDataMapQuest(url, item) {
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        var container = $(data).find('#schema-name-address');
        var name = $(container).find('h1[itemprop=name]').text();
        var address = $(container).find('span[itemprop=streetAddress]').text();
        var city = $(container).find('span[itemprop=addressLocality]').text();
        var state = $(container).find('span[itemprop=addressRegion]').text();
        var code = $(container).find('span[itemprop=postalCode]').text();
        changeDOMAddress(item, name, address, city, state, code);
    });
}

async function getDataYelp(url, item, loop) {
    // https://www.yelp.com/biz/law-office-g-randall-rostad-las-vegas
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        var name = $(data).find('.heading--h1__373c0__dvYgw.undefined.heading--inline__373c0__10ozy').text().trim();
        var fullAddress = $(data).find('.stickySidebar--fullHeight__373c0__1szWY.arrange-unit__373c0__o3tjT')
            .find('section.margin-b3__373c0__q1DuY.border-color--default__373c0__3-ifU')
            .find('.text__373c0__2Kxyz.text-color--subtle__373c0__3DZpi.text-align--left__373c0__2XGa-').first().text();
        if (!fullAddress) {
            fullAddress = $(data).find('section.margin-b3__373c0__q1DuY.border-color--default__373c0__3-ifU')
                .find('.text__373c0__2Kxyz.text-color--subtle__373c0__3DZpi.text-align--left__373c0__2XGa-').text();
        }
        if (!fullAddress && loop < 10) {
            console.log(loop);
            getDataYelp(url, item, loop++);
        }
        var address = '';
        var state = '';
        var code = '';
        if (fullAddress) {
            address = fullAddress.split(',')[0].trim();
            state = fullAddress.split(',')[1].trim().split(' ')[0].trim();
            code = fullAddress.split(',')[1].trim().split(' ')[1].trim();
        }
        changeDOMAddress(item, name, address, '', state, code);
    }).fail(function (error) {
        console.log(error);
    });
}

async function getDataYellowPages(url, item) {
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        console.log(data);

        var name = $(data).find('.sales-info').find('h1').text();
        console.log(name);
        var temp = $(data).find('.address').text();
        var address = $(temp).find('span').first().text();
        var cityPlus = $(temp).find('font').text();
        console.log(address);
        console.log(cityPlus);


    });
}

async function getDataDNB(url, item) {
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        var container = $(data).find('.content-body').find('.container');
        var name = $(container).find('.meta').find('.title').first().text();
        var address1 = $(container).find('.street_address_1').first().text().trim();
        var address2 = $(container).find('.street_address_2').first().text().trim();
        var city = $(container).find('.company_city').first().text();
        var state = $(container).find('.company_region').first().text();
        var code = $(container).find('.company_postal').first().text().split('-')[0].trim();
        changeDOMAddress(item, name, address1 + '' + address2, city, state, code);
    });
}

async function changeDOMAddress(item, name, address, city, state, code) {
    $(item).find('.street_missing').first().click();
    $(item).find('.city_missing').first().click();
    $(item).find('.state_missing').first().click();
    $(item).find('.postal_code_missing').first().click();

    $(item).css('background-color', '#99ff99');

    $(item).find('.business_name').val(name.toUpperCase().trim());
    $(item).find('.street').val(address.toUpperCase().trim());
    $(item).find('.city').val(city.toUpperCase().trim());
    $(item).find('.state').val(state.toUpperCase().trim());
    $(item).find('.postal_code').val(code.trim());
}

async function getAnswerAddress() {
    const object = {};
    object.sheetName = "Help Us Find Business Names And Addresses";
    object.size = 6;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        const website = $(item).find('a').first().attr('href').replace(googleTranslate, '');
        const name = $(item).find('.business_name').val();
        const address = $(item).find('.street').val();
        const city = $(item).find('.city').val();
        const state = $(item).find('.state').val();
        const code = $(item).find('.postal_code').val() + '';

        const key = [];
        key.push(website);

        const arr = [];
        if (name) {
            arr.push(name);
            if (address) {
                arr.push(address);
            } else {
                arr.push(null);
            }
            if (city) {
                arr.push(city);
            } else {
                arr.push(null);
            }
            if (state) {
                arr.push(state);
            } else {
                arr.push(null);
            }
            if (code) {
                arr.push(code);
            }
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// -----------------------------------------------Useful Product--------------------------------------------------------

async function completeTaskUsefulProduct() {
    var sheetId = 'ob86al9';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchId = $(item).attr('id');
        $('<span style="font-size: 16px; color: red;" class="answer">' + searchId + '</span><br/>').insertBefore($(item).find('.legend'));
        $(item).find('.cml_row').find('.how_useful_is_the_product_recommendation_on_the_right_to_the_customer')[2].click();
        var searchQuery = 'id==' + searchId;
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.cml_row').find('.how_useful_is_the_product_recommendation_on_the_right_to_the_customer')[entry[0].gsx$answer.$t - 1].click();
            }
        });
    });
}

// ------------------------------------------------Qcc Registry---------------------------------------------------------

async function CompleteTaskQccRegistry() {
    var sheetId = 'ome3dos';
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var name = $(item).find('div:contains("The security to research is:")').text().replace('The security to research is:', '').trim();
        name = name.replace('(', '').replace(')', '');
        $(item).find('.can_you_find_the_company').first().click();
        var searchQuery = 'name==' + name;
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$code.$t === 'No') {
                    $(item).find('.can_you_find_the_company').last().click();
                } else {
                    $(item).find('.paste_the_union_social_security_code__').val(entry[0].gsx$code.$t);
                    $(item).find('.paste_the_date_of_incorporation_').val(entry[0].gsx$date.$t);
                    $(item).find('.enter_legal_form').val(entry[0].gsx$type.$t);
                }
            } else {
                getAnswerQccRegistryFromPage(item, name);
            }
        });
    });
}

async function getAnswerQccRegistryFromPage(item, name) {
    var url = 'https://www.qcc.com/web/search?key=' + encodeURI(name);
    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        var searchName = $(data).find('.adsearch-list').find('.maininfo').first().find('span').first().text();
        searchName = searchName.replace('（', '').replace('）', '').trim();
        if (searchName === name) {
            var pageUrl = $(data).find('.adsearch-list').find('.maininfo').find('a').first().attr('href');
            getDataQccRegistry(item, pageUrl);
        } else {
            $(item).find('.can_you_find_the_company').last().click();
        }
    });
}

async function getDataQccRegistry(item, url) {
    console.log(url);

    $.ajax({
        type: "GET",
        url: url
    }).done(function (data) {
        var table = $(data).find('#Cominfo');
        var code = $(table).find('td:contains("统一社会信用代码")').nextAll('td').first().text().trim();
        var date = $(table).find('td:contains("成立日期")').nextAll('td').first().text().trim();
        var type = $(table).find('td:contains("企业类型")').nextAll('td').first().text().trim();
        if (code === '' || code === '-') {
            $(item).find('.paste_the_union_social_security_code__').val('NA');
        } else {
            $(item).find('.paste_the_union_social_security_code__').val(code);
        }
        if (date === '' || date === '-') {
            $(item).find('.paste_the_date_of_incorporation_').val('NA');
        } else {
            $(item).find('.paste_the_date_of_incorporation_').val(date);
        }
        if (type === '' || type === '-') {
            $(item).find('.enter_legal_form').val('NA');
        } else {
            $(item).find('.enter_legal_form').val(type);
        }
    });
}

async function getAnswerQccRegistry() {
    var object = new Object();
    object.sheetName = "Qcc Registry";
    object.size = 4;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var name = $(item).find('div:contains("The security to research is:")').text().replace('The security to research is:', '').trim();

        var key = [];
        key.push(name);

        var arr = [];
        if ($(item).find('.can_you_find_the_company:checked').val() === 'no') {
            arr.push('No');
        } else {
            arr.push($(item).find('.paste_the_union_social_security_code__').val());
            arr.push($(item).find('.paste_the_date_of_incorporation_').val());
            arr.push($(item).find('.enter_legal_form').val());
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });

    $.ajax({
        // crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: JSON.stringify(object),
        async: true,
        dataType: 'jsonp'
    });
}

// --------------------------------Help Us Find Company Information On Linkedin-----------------------------------------

async function CompleteTaskInformationLinkedIn() {
    var sheetId = 'oyhjol6';
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('a').first().removeClass('validates-clicked');
        var searchUrl = $(item).find('a').first().attr('href');
        $(item).find('.linkedin_yn').first().click();
        $(item).find('.company_size_yn').first().click();
        $(item).find('.company_founded_yn').first().click();
        $(item).find('.company_hq_yn').first().click();
        var searchQuery = 'searchurl==' + encodeURIComponent('\"' + searchUrl + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;

        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$url.$t === 'No') {
                    $(item).find('.linkedin_yn').last().click();
                } else {
                    $(item).find('.linkedin_url').val(entry[0].gsx$url.$t);
                    $(item).find('.linkedin_url').addClass('validation-passed');
                    $(item).find('.linkedin_url').removeClass('has_default');
                    if (entry[0].gsx$size.$t === 'No') {
                        $(item).find('.company_size_yn').last().click();
                    } else {
                        $(item).find('.company_size').val(entry[0].gsx$size.$t);
                    }
                    if (entry[0].gsx$founded.$t === 'No') {
                        $(item).find('.company_founded_yn').last().click();
                    } else {
                        $(item).find('.linkedin_founded_yr').val(entry[0].gsx$founded.$t);
                        $(item).find('.linkedin_founded_yr').addClass('validation-passed');
                        $(item).find('.linkedin_founded_yr').removeClass('has_default');
                    }
                    if (entry[0].gsx$address.$t === 'No') {
                        $(item).find('.company_hq_yn').last().click();
                    }
                }
            }
        });
    });
}

async function getAnswerInformationLinkedIn() {
    var object = new Object();
    object.sheetName = "Help Us Find Company Information On Linkedin";
    object.size = 5;
    object.encode = true;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchUrl = $(item).find('a').first().attr('href');
        var key = [];
        key.push(encodeURIComponent(searchUrl));

        var arr = [];
        if ($(item).find('.linkedin_yn:checked').val() === 'no') {
            arr.push('No');
        } else {
            arr.push($(item).find('.linkedin_url').val());
            if ($(item).find('.company_size_yn:checked').val() === 'no') {
                arr.push('No');
            } else {
                arr.push($(item).find('.company_size').val());
            }
            if ($(item).find('.company_founded_yn:checked').val() === 'no') {
                arr.push('No');
            } else {
                arr.push($(item).find('.linkedin_founded_yr').val());
            }
            if ($(item).find('.company_hq_yn:checked').val() === 'no') {
                arr.push('No');
            }
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });

    var dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// -------------------------------------Help Us Verify Our News Story Extractions---------------------------------------

async function CompleteTaskVerifyNewsStoryExtractions() {
    var sheetId = 'o1jsfs4';
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchUrl = $(item).find('a').first().attr('href');
        $(item).find('.linkopen').first().click();
        var searchQuery = 'url==' + encodeURIComponent('\"' + searchUrl + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;

        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$click.$t === 'No') {
                    $(item).find('.linkopen').last().click();
                } else {
                    if (entry[0].gsx$headline.$t === 'Yes') {
                        $(item).find('.headline_match').first().click();
                    }
                    if (entry[0].gsx$source.$t === 'Yes') {
                        $(item).find('.sourcematch').first().click();
                    }
                    if (entry[0].gsx$date.$t === 'Yes') {
                        $(item).find('.datematch').first().click();
                    }
                    if (entry[0].gsx$body.$t === 'Yes') {
                        $(item).find('.bodymatch').first().click();
                    }

                    if (entry[0].gsx$headline.$t === 'No') {
                        $(item).find('.headline_match').last().click();
                    }
                    if (entry[0].gsx$source.$t === 'No') {
                        $(item).find('.sourcematch').last().click();
                    }
                    if (entry[0].gsx$date.$t === 'No') {
                        $(item).find('.datematch').last().click();
                    }
                    if (entry[0].gsx$body.$t === 'No') {
                        $(item).find('.bodymatch').last().click();
                    }
                }
            }
        });
    });
}

async function getAnswerVerifyNewsStoryExtractions() {
    var object = new Object();
    object.sheetName = "Help Us Verify Our News Story Extractions";
    object.size = 6;
    object.encode = true;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchUrl = $(item).find('a').first().attr('href');

        var key = [];
        key.push(encodeURIComponent(searchUrl));

        var arr = [];
        if ($(item).find('.linkopen:checked').val() === 'No') {
            arr.push('No');
        } else {
            arr.push('Yes');
            if ($(item).find('.headline_match:checked').val() === 'No') {
                arr.push('No');
            } else {
                arr.push('Yes');
            }
            if ($(item).find('.sourcematch:checked').val() === 'No') {
                arr.push('No');
            } else {
                arr.push('Yes');
            }
            if ($(item).find('.datematch:checked').val() === 'No') {
                arr.push('No');
            } else {
                arr.push('Yes');
            }
            if ($(item).find('.bodymatch:checked').val() === 'No') {
                arr.push('No');
            } else {
                arr.push('Yes');
            }
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });

    var dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ------------------------------------Find Urls For A Business In A Certain Site---------------------------------------

async function CompleteTaskCertainSite() {
    var sheetId = 'ofgroea';
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var website, name, address;
        $(item).find('tr').each(function (id, it) {
            if (id === 0) {
                website = $(it).find('th').first().text().trim();
            }
            if (id === 1) {
                name = $(it).find('td').last().text().trim();
            }
            if (id === 2) {
                address = $(it).find('td').last().text().trim();
            }
        });

        $(item).find('a').first().removeClass('validates-clicked');
        $(item).find('.secondpart').css('display', 'block');
        $(item).find('.url_found').first().click();
        var searchQuery = 'address==' + encodeURIComponent('\"' + address + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                var hasData = false;
                for (let i = 0; i < entry.length; i++) {
                    const element = entry[i];
                    if (element.gsx$name.$t === name && element.gsx$website.$t === website) {
                        $(item).css('background-color', 'yellow');
                        if (entry[i].gsx$url.$t === 'No') {
                            $(item).find('.url_found').last().click();
                        } else {
                            $(item).find('.site_url').val(entry[i].gsx$url.$t);
                        }
                        hasData = true;
                        break;
                    }
                }
                if (!hasData) {
                    var searchUrl1 = $(item).find('a').first().attr('href');
                    var searchUrl2 = $(item).find('.secondpart').find('a').first().attr('href');
                    getDataCertainSite(index, searchUrl1, searchUrl2);
                }
            } else {
                var searchUrl1 = $(item).find('a').first().attr('href');
                var searchUrl2 = $(item).find('.secondpart').find('a').first().attr('href');
                getDataCertainSite(index, searchUrl1, searchUrl2);
            }
        });
    });
}

async function getDataCertainSite(index, url1, url2) {
    url1 = url1.replace('http', 'https');
    url2 = url2.replace('http', 'https');
    $.ajax({
        type: "GET",
        url: url1
    }).done(function (data) {
        var url = $(data).find(".g").first().find('a').attr('href');
        if (!url) {
            $.ajax({
                type: "GET",
                url: url2
            }).done(function (data) {
                var url = $(data).find(".g").first().find('a').attr('href');
                if (!url) {
                    updateViewCertainSite(index, null);
                } else {
                    updateViewCertainSite(index, url);
                }
            });
        } else {
            updateViewCertainSite(index, url);
        }
    });
}

async function updateViewCertainSite(rootIndex, data) {
    $('div[class="cml jsawesome"]').each(function (index, item) {
        if (index === rootIndex) {
            if (!data) {
                $(item).find('.url_found').last().click();
            } else {
                $(item).find('.site_url').val(data);
            }
        }
    });
}

async function getAnswerCertainSite() {
    var object = new Object();
    object.sheetName = "Find Urls For A Business In A Certain Site";
    object.size = 4;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var website, name, address;
        $(item).find('tr').each(function (id, it) {
            if (id === 0) {
                website = $(it).find('th').first().text().trim();
            }
            if (id === 1) {
                name = $(it).find('td').last().text().trim();
            }
            if (id === 2) {
                address = $(it).find('td').last().text().trim();
            }
        });
        var key = [];
        key.push(website);
        key.push(name);
        key.push(address);

        var arr = [];
        if ($(item).find('.url_found:checked').val() === 'no') {
            arr.push('No');
        } else {
            arr.push($(item).find('.site_url').val());
        }
        object.key.push(key);
        object.data.push(arr);
    });

    var dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// -------------------------------------Dataset: Guessed Domains - Rank Domains-----------------------------------------

async function completeTaskGuessedDomains() {
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var url = $(item).find(".fulldata").data("fulldata");
        var website1Button = $(item).find(".website_1");
        var website2Button = $(item).find(".website_2");
        $.getJSON(url, function (data) {
            $('<span style="font-size: 16px; color: red; margin-left: 10px">'
                + data.major_company_url.replace('www.', '').replace('http://', '').replace('https://', '').split(/[/?#]/)[0]
                + '</span><br/>').insertAfter(website1Button);
            $('<span style="font-size: 16px; color: red; margin-left: 10px">'
                + data.domain_candidate_url.replace('www.', '').replace('http://', '').replace('https://', '').split(/[/?#]/)[0]
                + '</span><br/>').insertAfter(website2Button);
        });
    });
}

// -----------------------------------Is The Object In The Box A Clear Query Image--------------------------------------

async function CompleteTaskClearQueryImage() {
    var sheetId = 'ofgroea';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var urlApi = $(item).find('img').first().attr('src');
        $('div[class="cml jsawesome"]').each(function (index, item) {
            if (index == 2) {
                $.ajax({
                    type: "GET",
                    url: urlApi,
                }).done(function (data, textStatus, request) {
                    // console.log(textStatus);
                    // console.log("--------------------");
                }).fail(function (error) {
                    console.log(error);
                });
            }
        });
    });
}

// ------------------------------------Help Us Describe The Person In The Photo-----------------------------------------

async function completeTaskDescribePersonInPhoto() {
    showSubmit();
    var sheetId = 'o3j4jqt';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchUrl = $(item).find('img').first().attr('src');
        searchUrl = encodeURIComponent('\"' + searchUrl + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "link==" + searchUrl;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.ppl_count[value="' + entry[0].gsx$people.$t + '"]').click();
                if (entry[0].gsx$people.$t == 'one_person') {
                    if (entry[0].gsx$whoole.$t) {
                        $(item).find('.person_whole[value="' + entry[0].gsx$whoole.$t + '"]').click();
                    }
                    if (entry[0].gsx$visible.$t) {
                        $(item).find('.person_visible[value="' + entry[0].gsx$visible.$t + '"]').click();
                    }
                    if (entry[0].gsx$light.$t) {
                        $(item).find('.lighting[value="' + entry[0].gsx$light.$t + '"]').click();
                    }
                    if (entry[0].gsx$small.$t) {
                        $(item).find('.person_small[value="' + entry[0].gsx$small.$t + '"]').click();
                    }
                    if (entry[0].gsx$adult.$t) {
                        $(item).find('.person_adult[value="' + entry[0].gsx$adult.$t + '"]').click();
                    }
                    if (entry[0].gsx$extreme.$t) {
                        $(item).find('.viewpoint[value="' + entry[0].gsx$extreme.$t + '"]').click();
                    }
                    if (entry[0].gsx$posing.$t) {
                        $(item).find('.person_on_object[value="' + entry[0].gsx$posing.$t + '"]').click();
                    }
                }
            } else {
                $(item).find('.ppl_count').first().click();
                $(item).find('.person_whole').first().click();
                $(item).find('.person_visible').first().click();
                $(item).find('.lighting').last().click();
                $(item).find('.person_small').last().click();
                $(item).find('.person_adult').first().click();
                $(item).find('.viewpoint').first().click();
                $(item).find('.person_on_object').last().click();
            }
        });
    });
}

async function getDataDescribePersonInPhoto() {
    var object = new Object();
    object.sheetName = "Help Us Describe The Person In The Photo";
    object.size = 9;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var link = $(item).find('img').first().attr('src');

        var key = [];
        key.push(link);

        var arr = [];
        var people = $(item).find('.ppl_count:checked').val();
        arr.push(people);
        if (people === 'one_person') {
            arr.push($(item).find('.person_whole:checked').val());
            arr.push($(item).find('.person_visible:checked').val());
            arr.push($(item).find('.lighting:checked').val());
            arr.push($(item).find('.person_small:checked').val());
            arr.push($(item).find('.person_adult:checked').val());
            arr.push($(item).find('.viewpoint:checked').val());
            arr.push($(item).find('.person_on_object:checked').val());
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });
    var dataSend = JSON.stringify(object);
    console.log(dataSend);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// --------------------------------------------Product Tips Updated-----------------------------------------------------

async function completeTaskProductTipsUpdated() {
    showSubmit();
    var sheetId = 'oos0ffz';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchId = $(item).attr('id');
        showId(item, searchId);
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "id==" + searchId;
        var sentence = $(item).find('.html-element-wrapper').first().find('div').last().text().trim().replace('Sentence: ', '').trim();
        sentence = encodeURIComponent(sentence);
        var translate = googleTranslateScript + '?text=' + sentence + '&from=en&to=vi';
        $.ajax({
            type: "GET",
            url: translate
        }).done(function (data) {
            console.log(data.responseText);
        }).fail(function (error) {
            $('<br/><span style="font-size: 20px; color: blue;">' + error.responseText + '</span><br/>').insertBefore($(item).find('.legend').first());
        });
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.is_this_sentence_a_useful_tip[value="' + entry[0].gsx$useful.$t + '"]').click();
                $(item).find('.does_this_sentence_encourage_unsafe_usage_of_the_product[value="' + entry[0].gsx$unsafe.$t + '"]').click();
                $(item).find('.at_which_stage_is_this_sentence_useful_you_may_select_more_than_one_answer[value="' + entry[0].gsx$stage1.$t + '"]').click();
                if (entry[0].gsx$stage2.$t) {
                    $(item).find('.at_which_stage_is_this_sentence_useful_you_may_select_more_than_one_answer[value="' + entry[0].gsx$stage2.$t + '"]').click();
                }
                if (entry[0].gsx$stage3.$t) {
                    $(item).find('.at_which_stage_is_this_sentence_useful_you_may_select_more_than_one_answer[value="' + entry[0].gsx$stage3.$t + '"]').click();
                }
            } else {
                $(item).find('.is_this_sentence_a_useful_tip').first().click();
                $(item).find('.does_this_sentence_encourage_unsafe_usage_of_the_product').last().click();
                $(item).find('.at_which_stage_is_this_sentence_useful_you_may_select_more_than_one_answer').last().click();
            }
        });
    });
}

async function getDataProductTipsUpdated() {
    var object = new Object();
    object.sheetName = "Product Tips Updated";
    object.size = 6;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var id = $(item).attr('id');

        var key = [];
        key.push(id);

        var arr = [];
        var useful = $(item).find('.is_this_sentence_a_useful_tip:checked').val();
        arr.push(useful);
        var unsafe = $(item).find('.does_this_sentence_encourage_unsafe_usage_of_the_product:checked').val();
        arr.push(unsafe);
        $(item).find(".at_which_stage_is_this_sentence_useful_you_may_select_more_than_one_answer:checked").each(function () {
            arr.push($(this).val());
        });

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });
    var dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ----------------------------Find Company Information - Revenue & Number Of Employees---------------------------------

async function completeTaskFindCompanyInformationRevenueNumberOfEmployees() {
    showSubmit();
    const sheetId = 'onb4ups';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('a').removeClass("validates-clicked");
        let searchUrl = $(item).find('a').first().attr('href');
        searchUrl = encodeURIComponent('\"' + searchUrl + '\"');
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "link1==" + searchUrl;
        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                const url1 = entry[0].gsx$url1.$t;
                const revenue1 = entry[0].gsx$revenue1.$t;
                if (url1) {
                    $(item).find('.gd_profile_yn').first().click();
                    $(item).find('.gd_url').val(url1);
                    $(item).find('.gd_url').removeClass('has_default');
                    $(item).find('.gd_url').addClass('validation-passed');
                }
                $(item).find('.gd_revenue_listed[value="' + revenue1 + '"]').click();
                if (revenue1 === 'range') {
                    $(item).find('.gd_lower_revenue').val(entry[0].gsx$from.$t);
                    $(item).find('.gd_upper_revenue').val(entry[0].gsx$to.$t);
                }
                if (entry[0].gsx$employee1.$t === 'no') {
                    $(item).find('.gd_employees_listed').last().click();
                } else {
                    $(item).find('.gd_employees_listed').first().click();
                    $(item).find('.gd_company_size').val(entry[0].gsx$employee1.$t);
                }
                $(item).find('.glassdoor_hq_yn[value="' + entry[0].gsx$address1.$t + '"]').click();

                const url2 = entry[0].gsx$url2.$t;
                if (url2) {
                    $(item).find('.owler_profile_yn').first().click();
                    $(item).find('.owler_url').val(url2);
                    $(item).find('.owler_url').removeClass('has_default');
                    $(item).find('.owler_url').addClass('validation-passed');
                }
                if (entry[0].gsx$revenue2.$t === 'no') {
                    $(item).find('.owler_revenue_listed').last().click();
                } else {
                    $(item).find('.owler_revenue_listed').first().click();
                    $(item).find('.owler_revenue').val(entry[0].gsx$revenue2.$t);
                }
                if (entry[0].gsx$employee2.$t === 'no') {
                    $(item).find('.owler_employees_listed').last().click();
                } else {
                    $(item).find('.owler_employees_listed').first().click();
                    $(item).find('.owler_company_size').val(entry[0].gsx$employee2.$t);
                }
                $(item).find('.owler_hq_yn[value="' + entry[0].gsx$address2.$t + '"]').click();
            }
        });
    });
}

async function getDataFindCompanyInformationRevenueNumberOfEmployees() {
    const object = {};
    object.sheetName = "Find Company Information - Revenue & Number Of Employees";
    object.size = 12;
    object.key = [];
    object.encode = true;
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        const key = [];
        key.push(encodeURIComponent($(item).find('.well').first().find('a').first().attr('href')));

        const arr = [];
        if ($(item).find('.gd_profile_yn:checked').val() === 'no') {
            for (let index = 0; index < 6; index++) {
                arr.push(null);
            }
        } else {
            arr.push($(item).find('.gd_url').val());
            const revenue1 = $(item).find('.gd_revenue_listed:checked').val();
            arr.push(revenue1);
            if (revenue1 === 'range') {
                arr.push($(item).find('.gd_lower_revenue').val());
                arr.push($(item).find('.gd_upper_revenue').val());
            } else {
                arr.push(null);
                arr.push(null);
            }
            if ($(item).find('.gd_employees_listed:checked').val() === 'no') {
                arr.push('no');
            } else {
                arr.push($(item).find('.gd_company_size').val());
            }
            arr.push($(item).find('.glassdoor_hq_yn:checked').val());
        }

        arr.push($(item).find('.well').last().find('a').first().attr('href'));
        if ($(item).find('.owler_profile_yn:checked').val() === 'yes') {
            arr.push($(item).find('.owler_url').val());
            if ($(item).find('.owler_revenue_listed:checked').val() === 'no') {
                arr.push(null);
            } else {
                arr.push($(item).find('.owler_revenue').val());
            }
            if ($(item).find('.owler_employees_listed:checked').val() === 'no') {
                arr.push('no');
            } else {
                arr.push($(item).find('.owler_company_size').val());
            }
            arr.push($(item).find('.owler_hq_yn:checked').val());
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------------------Collect Phone Number----------------------------------------------------

async function completeTaskCollectPhoneNumber() {
    showSubmit();
    let elements = $('div[class="cml jsawesome"]');
    const key = [];
    elements.each(function (index, item) {
        let searchUrl = $(item).find('a').first().attr('href');
        const itemKey = [];
        itemKey.push(searchUrl);
        key.push(itemKey);
    });
    const promise = getAnswer("Collect Phone Number", key);
    promise.done(function (data) {
        $('div[class="cml jsawesome"]').each(function (index, item) {
            let searchUrl = $(item).find('a').first().attr('href');
            searchUrl = searchUrl.replace(/.*\//, '');
            $.each(data, function (i, val) {
                if (val[0] === searchUrl) {
                    $(item).css('background-color', 'yellow');
                    changeDomLook(index, val[1]);
                }
            })
        });
        elements.each(function (index, item) {
            let searchUrl = $(item).find('a').first().attr('href');
            $.each(data, function (i, val) {
                if (val[0] === searchUrl) {
                    $(item).css('background-color', 'yellow');
                    if (val[1] === 'No') {
                        $(item).find('.mult_locations_yn').last().click();
                    } else {
                        $(item).find('.mult_locations_yn').first().click();
                        $(item).find('.num_locations').val(val[1]);
                    }
                    if (val[2] === 'No') {
                        $(item).find('.phone_yn').last().click();
                        if (val[2]) {
                            $(item).find('.new_url_yn').first().click();
                            $(item).find('.new_url').val(val[3]);
                            $(item).find('.phone').last().val(val[4]);
                        } else {
                            $(item).find('.new_url_yn').last().click();
                        }
                    } else {
                        $(item).find('.phone_yn').first().click();
                        $(item).find('.phone').first().val(val[2]);
                    }
                }
            })
        })
    });
}

async function getDataCollectPhoneNumber() {
    const object = {};
    object.sheetName = "Collect Phone Number";
    object.size = 5;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        const link = $(item).find('a').first().attr('href');

        const key = [];
        key.push(link);

        const arr = [];
        if ($(item).find('.mult_locations_yn:checked').val() === 'no') {
            arr.push('No');
        } else {
            arr.push($(item).find('.num_locations').val());
        }
        if ($(item).find('.phone_yn:checked').val() === 'yes') {
            arr.push($(item).find('.phone').first().val());
        } else {
            arr.push('No');
            if ($(item).find('.new_url_yn:checked').val() === 'yes') {
                arr.push($(item).find('.new_url').val());
                arr.push($(item).find('.phone_searched').last().val());
            }
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }
        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// -------------------------------Priority Work - Quick Job: Find Website Links For Corporations (Prod)-----------------

async function completeTaskFindWebsiteLinksForCorporations() {
    var sheetId = 'oj0g2bz';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('.website_open').first().click();
        $(item).find('.exec_find').first().click();
        $(item).find('.board_find').first().click();
        var name = $(item).find('h2').first().text();
        searchName = encodeURIComponent('\"' + name + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "name==" + searchName;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.exec_link').val(entry[0].gsx$leadership.$t);
                $(item).find('.board_link ').last().val(entry[0].gsx$directors.$t);
            }
            ;
        });
    });
}

// -------------------------------Answer The Questions About The Person In The Photo------------------------------------

async function completeTaskAnswerTheQuestionsAboutThePersonInThePhoto() {
    showSubmit();
    var sheetId = 'oy6bkjo';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('.orientation').first().click();
        $(item).find('.standing').first().click();
        $(item).find('.prominent_object').last().click();
        $(item).find('.holding_object').last().click();
        $(item).find('.gender').last().click();
        $(item).find('.fitted_clothing').first().click();
        $(item).find('.hair_headwear').first().click();
        var imageURL = $(item).find('img').first().attr('src');
        searchUrl = encodeURIComponent('\"' + imageURL + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "link==" + searchUrl;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.orientation[value="' + entry[0].gsx$orientation.$t + '"]').click();
                $(item).find('.standing[value="' + entry[0].gsx$standing.$t + '"]').click();
                $(item).find('.prominent_object[value="' + entry[0].gsx$prominent.$t + '"]').click();
                $(item).find('.holding_object[value="' + entry[0].gsx$holding.$t + '"]').click();
                $(item).find('.gender[value="' + entry[0].gsx$gender.$t + '"]').click();
                $(item).find('.fitted_clothing[value="' + entry[0].gsx$body.$t + '"]').click();
                $(item).find('.hair_headwear[value="' + entry[0].gsx$wearing.$t + '"]').click();
            }
        });
    });
}

async function getDataAnswerTheQuestionsAboutThePersonInThePhoto() {
    var object = {};
    object.sheetName = "Answer The Questions About The Person In The Photo";
    object.size = 8;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var imageURL = $(item).find('img').first().attr('src');

        var key = [];
        key.push(imageURL);

        var arr = [];
        arr.push($(item).find('.orientation:checked').val());
        arr.push($(item).find('.standing:checked').val());
        arr.push($(item).find('.prominent_object:checked').val());
        arr.push($(item).find('.holding_object:checked').val());
        arr.push($(item).find('.gender:checked').val());
        arr.push($(item).find('.fitted_clothing:checked').val());
        arr.push($(item).find('.hair_headwear:checked').val());

        object.key.push(key);
        object.data.push(arr);
    });
    var dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------------Human Vs Machine Generated Poems----------------------------------------------

async function completeTaskHumanVsMachineGeneratedPoems() {
    showSubmit();
    var sheetId = 'o3rfsr6';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        $(item).find('.answer').last().click();
        var link = $(item).find('a').first().attr('href');
        link = encodeURIComponent('\"' + link + '\"');
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "link==" + link;
        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$value.$t === "1") {
                    $(item).find('.answer').first().click();
                }
            }
        });
    });
}

async function getDataHumanVsMachineGeneratedPoems() {
    const object = {};
    object.sheetName = "Human Vs Machine Generated Poems";
    object.size = 2;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var link = $(item).find('a').first().attr('href');

        var key = [];
        key.push(link);

        var arr = [];
        if ($(item).find('.answer:checked').val() === '1') {
            arr.push('1');
        } else {
            arr.push('2');
        }

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ------------------------------------------------M2m Profanity--------------------------------------------------------

async function completeTaskM2mProfanity() {
    var sheetId = 'os8gjly';
    $('div[class="cml jsawesome"]').each(function (index, item) {
        const searchId = $(item).attr('id');
        showId(item, searchId);
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "id==" + searchId;

        $(item).find('.obscene').last().click();
        $(item).find('.insult').last().click();
        $(item).find('.identity_attack').last().click();
        $(item).find('.threat').last().click();

        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                if (entry[0].gsx$obscene.$t === "y") {
                    $(item).find('.obscene').first().click();
                }
                if (entry[0].gsx$insult.$t === "y") {
                    $(item).find('.insult').first().click();
                }
                if (entry[0].gsx$attack.$t === "y") {
                    $(item).find('.identity_attack').first().click();
                }
                if (entry[0].gsx$threat.$t === "y") {
                    $(item).find('.threat').first().click();
                }
            }
        });
    });
}

// ----------------------------------------Verify Paraphrase Of Question------------------------------------------------

async function completeTaskVerifyParaphraseOfQuestion() {
    let sheetId = "otu2rgv";
    $('div[class="cml jsawesome"]').each(function (index, item) {
        var searchId = $(item).attr('id');
        showId(item, searchId);
        var url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "id==" + searchId;

        $(item).find('.is_b_a_good_paraphrase_of_a').first().click();

        $.getJSON(url, function (data) {
            var entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.is_b_a_good_paraphrase_of_a').last().click();
            }
        });
    });
}

// ----------------------------------------Classify Youtube Channels----------------------------------------------------

async function completeTaskClassifyYoutubeChannels() {
    let sheetId = "oyoiob1";
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let searchUrl = $(item).find('a').first().attr('href');
        searchUrl = encodeURIComponent('\"' + searchUrl + '\"');
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + "link==" + searchUrl;
        $(item).find('a').first().removeClass('validates-clicked')

        let title = $('.job-title').text().replace('Classify Youtube Channels For ', '');

        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                for (let i = 0; i < entry.length; i++) {
                    const element = entry[i];
                    if (element.gsx$type.$t === title) {
                        $(item).css('background-color', 'yellow');
                        $(item).find('.rating_name[value="' + element.gsx$value.$t + '"]').click();
                        break;
                    }
                }
            }
        });
    });
}

async function getDataClassifyYoutubeChannels() {
    const object = {};
    object.sheetName = "Classify Youtube Channels";
    object.size = 3;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let searchUrl = $(item).find('a').first().attr('href');
        let title = $('.job-title').text().replace('Classify Youtube Channels For ', '');
        const key = [];
        key.push(searchUrl);
        key.push(title);

        const arr = [];
        arr.push($(item).find('.rating_name:checked').val())

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// --------------------------------------------Name Categorization------------------------------------------------------

async function completeTaskNameCategorization() {
    let sheetId = "o2lz12p";
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let name = $(item).find('p').first().text().replaceAll('Name: ', '');
        name = encodeURIComponent('\"' + name + '\"');
        const searchQuery = 'name==' + name;
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;
        $(item).find('a').first().removeClass('validates-clicked')

        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.is_this_a_name_of_a_person_or_company[value="' + entry[0].gsx$value.$t + '"]').click();
            }
        });
    });
}

async function getDataNameCategorization() {
    const object = {};
    object.sheetName = "Name Categorization";
    object.size = 2;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let name = $(item).find('p').first().text().replaceAll('Name: ', '');
        const key = [];
        key.push(name);

        const arr = [];
        arr.push($(item).find('.is_this_a_name_of_a_person_or_company:checked').val())

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// -----------------------------------------Easy Image Labeling Task----------------------------------------------------

async function completeTaskEasyImageLabelingTask() {
    let sheetId = "owzy5t6";
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let imageUrl = $(item).find('img').first().attr('src');
        imageUrl = encodeURIComponent('\"' + imageUrl + '\"');
        const searchQuery = 'url==' + imageUrl;
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;
        $(item).find('.is_it_a_mixed_lighting_image').last().click();
        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                $(item).css('background-color', 'yellow');
                $(item).find('.is_it_a_mixed_lighting_image[value="' + entry[0].gsx$value.$t + '"]').click();
            }
        });
    });
}

async function getDataEasyImageLabelingTask() {
    const object = {};
    object.sheetName = "Easy Image Labeling Task";
    object.size = 2;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let imageUrl = $(item).find('img').first().attr('src');
        const key = [];
        key.push(imageUrl);

        const arr = [];
        arr.push($(item).find('.is_it_a_mixed_lighting_image:checked').val())

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------Find The Official Website Of Businesses---------------------------------------------

async function completeTaskFindTheOfficialWebsiteOfBusinesses() {
    showSubmit();
    let elements = $('div[class="cml jsawesome"]');
    const key = [];
    elements.each(function (index, item) {
        let name = $(item).find('p').first().text().replaceAll('1.', '').replaceAll('Business name:', '').trim();
        let address = $(item).find('p').first().next().text().replaceAll('2.', '').replaceAll('Business address:', '').trim();
        const itemKey = [];
        itemKey.push(name);
        itemKey.push(address);
        key.push(itemKey);

        $(item).find('a').each(function (id, it) {
            $(it).removeClass('validates-clicked');
        })
        $(item).find('.company_match').first().click();
        $(item).find('.active_domain').first().click();
    });
    const promise = getAnswer("Find The Official Website Of Businesses", key);
    promise.done(function (data) {
        $('div[class="cml jsawesome"]').each(function (index, item) {
            let name = $(item).find('p').first().text().replaceAll('1.', '').replaceAll('Business name:', '').trim();
            let address = $(item).find('p').first().next().text().replaceAll('2.', '').replaceAll('Business address:', '').trim();
            $.each(data, function (i, val) {
                if (val[0] === name && val[1] === address) {
                    $(item).css('background-color', 'yellow');
                    if (val[2] === 'no') {
                        $(item).find('.company_match').last().click();
                    } else {
                        $(item).find('.domain').val(val[3]);
                        $(item).find('.active_domain[value="' + val[4] + '"]').click();
                    }
                }
            })
        });
    });
}

async function getDataFindTheOfficialWebsiteOfBusinesses() {
    const object = {};
    object.sheetName = "Find The Official Website Of Businesses";
    object.size = 5;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        const key = [];
        let name = $(item).find('p').first().text().replaceAll('1.', '').replaceAll('Business name:', '').trim();
        let address = $(item).find('p').first().next().text().replaceAll('2.', '').replaceAll('Business address:', '').trim();
        key.push(name);
        key.push(address);

        const arr = [];

        let domain = $(item).find('.domain').val();

        if (domain == null || domain === '') {
            $(item).find('.company_match').last().click();
        }

        let match = $(item).find('.company_match:checked').val();
        arr.push(match);
        arr.push(domain);
        arr.push($(item).find('.active_domain:checked').val());

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------------------------Tiktoks-----------------------------------------------------------

async function completeTaskTiktoks() {
    let sheetId = "ouqky7t";
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let id = $(item).find('video').first().attr('id');
        const searchQuery = 'id==' + id;
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;

        $('<span style="font-size: 16px; color: red; max-width: 200px; word-wrap: break-word; display: block;" class="answer">'
            + id + '</span><br/>').insertBefore($(item).find('.video-question-sub').first());

        let title = $('.job-title').text();

        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                for (let i = 0; i < entry.length; i++) {
                    const element = entry[i];
                    if (element.gsx$type.$t === title) {
                        $(item).css('background-color', 'yellow');
                        $(item).find('input[value="' + element.gsx$value.$t + '"]').click();
                        break;
                    }
                }
            }
        });
    });
}

async function getDataTiktoks() {
    const object = {};
    object.sheetName = "Tiktoks";
    object.size = 3;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let id = $(item).find('video').first().attr('id');
        let title = $('.job-title').text();
        const key = [];
        key.push(id);
        key.push(title);

        const arr = [];
        arr.push($(item).find('input:checked').val());

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------------------------Orginfer----------------------------------------------------------

async function completeTaskOrginfer() {
    let sheetId = "ok1gunr";
    showSubmit();
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let name = $(item).find('.span3').first().text().trim().split(/\r?\n/)[0].trim();
        let link = $(item).find('a').first().attr('href');
        const searchQuery = 'link==' + encodeURIComponent('\"' + link + '\"');
        const url = sheetUrl + spreadsheetID + "/" + sheetId + sheetParam + searchQuery;

        $.getJSON(url, function (data) {
            const entry = data.feed.entry;
            if (entry) {
                for (let i = 0; i < entry.length; i++) {
                    const element = entry[i];
                    if (element.gsx$name.$t === name) {
                        $(item).css('background-color', 'yellow');
                        $(item).find('input[value="' + element.gsx$value.$t + '"]').click();
                        break;
                    }
                }
            }
        });
    });
}

async function getDataOrginfer() {
    const object = {};
    object.sheetName = "Orginfer";
    object.size = 3;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        let name = $(item).find('.span3').first().text().trim().split(/\r?\n/)[0].trim();
        let link = $(item).find('a').first().attr('href');
        const key = [];
        key.push(name);
        key.push(link);

        const arr = [];
        arr.push($(item).find('input:checked').val());

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// ---------------------------------------Does The Company Own The Brand------------------------------------------------

async function completeTaskDoesTheCompanyOwnTheBrand() {
    showSubmit();
    let elements = $('div[class="cml jsawesome"]');
    const key = [];
    elements.each(function (index, item) {
        let company = $(item).find('.span10').first().find('span').first().text().trim();
        let product = $(item).find('.span10').first().children('span').eq(1).text().trim();
        const itemKey = [];
        itemKey.push(company);
        itemKey.push(product);
        key.push(itemKey);

        $(item).find('.related').first().click();
        $(item).find('.evidence').first().click();
    });
    const promise = getAnswer("Does The Company Own The Brand", key);
    promise.done(function (data) {
        $('div[class="cml jsawesome"]').each(function (index, item) {
            let company = $(item).find('.span10').first().find('span').first().text().trim();
            let product = $(item).find('.span10').first().children('span').eq(1).text().trim();
            $.each(data, function (i, val) {
                if (val[0] === company && val[1] === product) {
                    $(item).css('background-color', 'yellow');
                    if (val[2] === 'no') {
                        $(item).find('.related').last().click();
                    } else {
                        $(item).children('.evidence').eq(val[3] - 1).click();
                        let evidence = val[3];
                        if (evidence === '1') {
                            $(item).find('input[value="yes_primary_source"]').click();
                        } else if (evidence === '2') {
                            $(item).find('input[value="yes_secondary_source"]').click();
                        } else {
                            $(item).find('input[value="cannot_find_link between them"]').click();
                        }
                        if (evidence !== '3') {
                            $(item).find('.website').val(val[4]);
                        }
                    }
                }
            })
        });
    });
}

async function getDataDoesTheCompanyOwnTheBrand() {
    const object = {};
    object.sheetName = "Does The Company Own The Brand";
    object.size = 5;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        const key = [];
        let company = $(item).find('.span10').first().find('span').first().text().trim();
        let product = $(item).find('.span10').first().children('span').eq(1).text().trim();
        key.push(company);
        key.push(product);

        const arr = [];

        let related = $(item).find('.related:checked').val();
        arr.push(related);

        if (related === 'yes') {
            let evidence = $(item).find('.evidence:checked').val();
            let value;
            if (evidence === 'yes_primary_source') {
                value = '1';
            } else if (evidence === 'yes_secondary_source') {
                value = '2';
            } else {
                value = '3';
            }
            arr.push(value);
            if (value !== '3') {
                arr.push($(item).find('.website').val())
            }
        }

        if (arr.length < object.size - key.length) {
            for (let index = arr.length; index < object.size - key.length; index++) {
                arr.push(null);
            }
        }

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

//----------------------------------------Rate Content Of Animated Gifs-------------------------------------------------

async function completeTaskRateContentOfAnimatedGifs() {
    showSubmit();
    let elements = $('div[class="cml jsawesome"]');
    const key = [];
    elements.each(function (index, item) {
        let link = $(item).find('a').first().attr('href');
        const itemKey = [];
        itemKey.push(link);
        key.push(itemKey);
    });
    const promise = getAnswer("Rate Content Of Animated Gifs", key);
    promise.done(function (data) {
        $('div[class="cml jsawesome"]').each(function (index, item) {
            let link = $(item).find('a').first().attr('href');
            $.each(data, function (i, val) {
                if (val[0] === link) {
                    $(item).css('background-color', 'yellow');
                    let index = val[1] - 1;
                    $(item).find('input.category:eq(' + index + ')').click();
                }
            })
        });
    });
}

async function getDataRateContentOfAnimatedGifs() {
    const object = {};
    object.sheetName = "Rate Content Of Animated Gifs";
    object.size = 2;
    object.key = [];
    object.data = [];
    $('div[class="cml jsawesome"]').each(function (index, item) {
        const key = [];
        let link = $(item).find('a').first().attr('href');
        key.push(link);

        const arr = [];

        $(item).find('.category').each(function (i) {
            if (this.checked) {
                arr.push(i + 1);
                return false;
            }
        })

        object.key.push(key);
        object.data.push(arr);
    });
    const dataSend = JSON.stringify(object);

    $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// --------------------------------------------------Get answer---------------------------------------------------------

function getAnswer(sheetName, key) {
    const object = {};
    object.sheetName = sheetName;
    object.key = key;
    object.find = true;

    const dataSend = JSON.stringify(object);

    return $.ajax({
        crossDomain: true,
        url: googleScriptLink,
        method: "POST",
        data: dataSend,
        async: true,
        dataType: 'json'
    });
}

// -------------------------------------------------Show widget---------------------------------------------------------

function showId(item, id) {
    $('<span style="font-size: 16px; color: red; max-width: 200px; word-wrap: break-word; display: block;" class="answer">'
        + id + '</span><br/>').insertBefore($(item).find('.legend').first());
}

function showSubmit() {
    $('<input type="button" id="mySubmit" value="Submit ở đây" class="btn-cf-blue"/>').insertBefore($('.btn-cf-blue'));
}

// ---------------------------------------------------Click-------------------------------------------------------------

window.onclick = function (event) {
    const target = event.target;
    if (target.matches('.answer')) {
        const $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(target).text()).select();
        document.execCommand("copy");
        $temp.remove();
    } else if (target.matches('li') || target.matches('span')) {
        setSecondInputLook(target);
    } else if (target.matches('#mySubmit')) {
        const index = checkTask();
        switch (index) {
            case 2:
            case 3:
                getAnswerAmazon();
                break;
            case 11:
                getDataTradingCard();
                break;
            case 13:
                getAnswerDup();
                break;
            case 14:
                getAnswerAddress();
                break;
            case 16:
                getAnswerQccRegistry();
                break;
            case 17:
                getAnswerInformationLinkedIn();
                break;
            case 18:
                getAnswerVerifyNewsStoryExtractions();
                break;
            case 19:
                getAnswerCertainSite();
                break;
            case 22:
                getDataDescribePersonInPhoto();
                break;
            case 23:
                getDataProductTipsUpdated();
                break;
            case 24:
                getDataCollectPhoneNumberAndNumberOfLocationsFromWebsites();
                break;
            case 25:
                getDataCollectPhoneNumber();
                break;
            case 26:
                getDataFindCompanyInformationRevenueNumberOfEmployees();
                break;
            case 28:
                getDataAnswerTheQuestionsAboutThePersonInThePhoto();
                break;
            case 29:
                getDataHumanVsMachineGeneratedPoems();
                break;
            case 30:
                getDataFindTheOfficialWebsiteOfBusinesses();
                break;
            case 33:
                getDataClassifyYoutubeChannels();
                break;
            case 34:
                getDataNameCategorization();
                break;
            case 35:
                getDataEasyImageLabelingTask();
                break;
            case 36:
                getDataTiktoks();
                break;
            case 37:
                getDataOrginfer();
                break;
            case 38:
                getDataDoesTheCompanyOwnTheBrand();
                break;
            case 39:
                getDataRateContentOfAnimatedGifs();
                break;
            default:
                break;
        }
    }
}

// ---------------------------------------------------Scroll------------------------------------------------------------

function scrollToEnd() {
    $(document).scrollTop($(document).height());
}

$(document).ready(async function () {
    const error = $('#job_units_missed');
    if (error.length) {
        let t = new Audio(chrome.runtime.getURL("data/sound.ogg"));
        await t.play();
        delete t;
        await getAnswerLook(error);
    } else {
        const index = checkTask();
        switch (index) {
            case 0:
            case 1:
                callAjax(index);
                break;
            case 2:
                $('<span id="loop" style="font-size: 50px; color: green;">0</span>').insertAfter($('.job-title'));
                $('<span id="error" style="font-size: 50px; color: red;">0</span>').insertAfter($('#loop'));
                completeTaskAmazon();
                break;
            case 3:
                $('<span id="loop" style="font-size: 50px; color: green;">0</span>').insertAfter($('.job-title'));
                $('<span id="error" style="font-size: 50px; color: red;">0</span>').insertAfter($('#loop'));
                completeTaskAmazonMexico(index);
                break;
            case 4:
            case 5:
                completeTaskLook();
                break
            case 6:
            case 7:
                callAjaxProd(index);
                break
            case 8:
                completeTaskDocument();
                break;
            case 9:
                completeTaskIdentifyImage();
                break;
            case 10:
                completeTaskHelpUsFindOfficial();
                break;
            case 11:
                completeTradingCard();
                break;
            case 12:
                completeProductSubstitution();
                break;
            case 13:
                completeTaskDup();
                break;
            case 14:
                completeTaskAddress();
                break;
            case 15:
                completeTaskUsefulProduct();
                break;
            case 16:
                CompleteTaskQccRegistry();
                break;
            case 17:
                CompleteTaskInformationLinkedIn();
                break;
            case 18:
                CompleteTaskVerifyNewsStoryExtractions();
                break;
            case 19:
                CompleteTaskCertainSite();
                break;
            case 20:
                completeTaskGuessedDomains();
                break;
            case 21:
                CompleteTaskClearQueryImage();
                break;
            case 22:
                completeTaskDescribePersonInPhoto();
                break;
            case 23:
                completeTaskProductTipsUpdated();
                break;
            case 25:
                completeTaskCollectPhoneNumber();
                break;
            case 26:
                completeTaskFindCompanyInformationRevenueNumberOfEmployees();
                break;
            case 27:
                completeTaskFindWebsiteLinksForCorporations();
                break;
            case 28:
                completeTaskAnswerTheQuestionsAboutThePersonInThePhoto();
                break;
            case 29:
                completeTaskHumanVsMachineGeneratedPoems();
                break;
            case 30:
                completeTaskFindTheOfficialWebsiteOfBusinesses();
                break;
            case 31:
                completeTaskM2mProfanity();
                break;
            case 32:
                completeTaskVerifyParaphraseOfQuestion();
                break;
            case 33:
                completeTaskClassifyYoutubeChannels();
                break;
            case 34:
                completeTaskNameCategorization();
                break;
            case 35:
                completeTaskEasyImageLabelingTask();
                break;
            case 36:
                completeTaskTiktoks();
                break;
            case 37:
                completeTaskOrginfer();
                break;
            case 38:
                completeTaskDoesTheCompanyOwnTheBrand();
                break;
            case 39:
                completeTaskRateContentOfAnimatedGifs();
                break;
            default:
                break;
        }
    }
});

function checkTask() {
    const title = $('.job-title').text();
    if (/In What Industry This Company Work/.test(title)) {
        return 0;
    }
    if (/Where Is This Publisher From/.test(title)) {
        return 1;
    }
    if (/Extract Amazon Sales Rank For/.test(title)) {
        return 2;
    }
    if (/Extract Amazon Mexico Sales Rank For/.test(title)) {
        return 3;
    }
    if (/Look At Advertisements And Judge Them/.test(title)) {
        return 4;
    }
    if (/Olhe O Anúncio E Dê Sua Opinião/.test(title)) {
        return 5;
    }
    if (/Prod - Persona Level Validation/.test(title)) {
        return 6;
    }
    if (/Prod - Account Level Validation/.test(title)) {
        return 7;
    }
    if (/Document Recognition And Translations/.test(title)) {
        return 8;
    }
    if (/Identify Tops’ Pattern In Images/.test(title)) {
        return 9;
    }
    if (/Help Us Find Official\/Directory Websites/.test(title)) {
        return 10;
    }
    if (/Trading Cards/.test(title)) {
        return 11;
    }
    if (/Product Substitution/.test(title)) {
        return 12;
    }
    if (/Duplicate Fashion Product Identification Task/.test(title)) {
        return 13;
    }
    if (/Help Us Find Business Names And Addresses/.test(title)) {
        return 14;
    }
    if (/Useful Product Recommendation/.test(title)) {
        return 15;
    }
    if (/Qcc Registry/.test(title)) {
        return 16;
    }
    if (/Help Us Find Company Information On Linkedin/.test(title)) {
        return 17;
    }
    if (/Help Us Verify Our News Story Extractions/.test(title)) {
        return 18;
    }
    if (/Find Urls For A Business In A Certain Site/.test(title)) {
        return 19;
    }
    if (/Dataset: Guessed Domains - Rank Domains/.test(title)) {
        return 20;
    }
    if (/Is The Object In The Box A Clear Query Image/.test(title)) {
        return 21;
    }
    if (/Help Us Describe The Person In The Photo/.test(title)) {
        return 22;
    }
    if (/Product Tips Updated/.test(title)) {
        return 23;
    }
    if (/Collect Phone Number/.test(title)) {
        return 25;
    }
    if (/Find Company Information - Revenue & Number Of Employees/.test(title)) {
        return 26;
    }
    if (/Find Website Links For Corporations/.test(title)) {
        return 27;
    }
    if (/Answer The Questions About The Person In The Photo/.test(title)) {
        return 28;
    }
    if (/Human Vs Machine Generated Poems/.test(title)) {
        return 29;
    }
    if (/Find The Official Website Of Businesses/.test(title)) {
        return 30;
    }
    if (/M2m Profanity/.test(title)) {
        return 31;
    }
    if (/Verify Paraphrase Of Question/.test(title)) {
        return 32;
    }
    if (/Classify Youtube Channels For Fake News Content/.test(title)) {
        return 33;
    }
    if (/Name Categorization/.test(title)) {
        return 34;
    }
    if (/Easy Image Labeling Task/.test(title)) {
        return 35;
    }
    if (/Do These Tiktoks Contain/.test(title) || /Does This Tiktok Contain/.test(title)
        || /Do These Tiktok's Contain/.test(title) || /Are These Tiktoks/.test(title)) {
        return 36;
    }
    if (/Orginfer/.test(title)) {
        return 37;
    }
    if (/Does The Company Own The Brand/.test(title)) {
        return 38;
    }
    if (/Rate Content Of Animated Gifs/.test(title)) {
        return 39;
    }
    return null;
}
